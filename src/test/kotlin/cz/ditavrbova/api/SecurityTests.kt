package cz.ditavrbova.api

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.security.test.context.support.WithAnonymousUser
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.reactive.server.WebTestClient
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@SpringBootTest
@Testcontainers
@AutoConfigureWebTestClient
class SecurityTests {

    @Suppress("unused")
    companion object {
        @Container
        @ServiceConnection
        val mongo: MongoDBContainer = MongoDBContainer("mongo:latest")
    }

    @Autowired
    private lateinit var client: WebTestClient

    @Test
    @WithAnonymousUser
    fun `test that homepage can be accessed without authentication`() {
        client.get().uri("/").exchange().expectStatus().isNotFound
    }

    @Test
    @WithAnonymousUser
    fun `test that api cannot be accessed without authentication`() {
        client.get().uri("/api/v1/").exchange().expectStatus().isUnauthorized
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `test that homepage can be accessed with authentication`() {
        client.get().uri("/").exchange().expectStatus().isNotFound
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `test that api can be accessed with authentication`() {
        client.get().uri("/api/v1/").exchange().expectStatus().isNotFound
    }
}