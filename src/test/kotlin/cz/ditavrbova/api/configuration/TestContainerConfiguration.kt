package cz.ditavrbova.api.configuration

import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.testcontainers.containers.MongoDBContainer

class TestContainerConfiguration : ApplicationContextInitializer<ConfigurableApplicationContext>{

    private val mongo: MongoDBContainer = MongoDBContainer("mongo:latest").withExposedPorts(27017)

    override fun initialize(context: ConfigurableApplicationContext) {
        mongo.start()

        // Setup the testcontainer mongodb URI
        TestPropertyValues
            .of("spring.data.mongodb.uri=${mongo.getReplicaSetUrl("ditavrbova-test")}")
            .applyTo(context.environment)
    }
}