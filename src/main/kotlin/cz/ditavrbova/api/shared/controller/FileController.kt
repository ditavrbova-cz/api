package cz.ditavrbova.api.shared.controller

import cz.ditavrbova.api.shared.dto.UploadedFileDto
import cz.ditavrbova.api.shared.dto.toDto
import cz.ditavrbova.api.shared.service.FileService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.util.UUID

@RestController
@RequestMapping("/api/v1/files")
class FileController(private val service: FileService) {

    @GetMapping("/list")
    suspend fun list(): ResponseEntity<List<UploadedFileDto>> {
        val documents = service.listFiles()
        val response = documents.map { it.toDto() }

        return ResponseEntity.ok(response)
    }

    @PostMapping("/upload", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    suspend fun upload(@RequestParam("file") file: MultipartFile): ResponseEntity<UploadedFileDto> {
        val document = service.uploadFile(file)
        val response = document.toDto()

        return ResponseEntity.ok(response)
    }

    @PostMapping("/delete/{id}")
    suspend fun delete(@PathVariable("id") id: UUID): ResponseEntity<Unit> {
        return ResponseEntity.status(
            if (service.deleteFile(id)) HttpStatus.ACCEPTED
            else HttpStatus.NOT_FOUND
        ).build()
    }
}