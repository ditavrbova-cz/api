package cz.ditavrbova.api.shared.controller

import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/api/v1/authentication")
class AuthenticationController {

    @PostMapping("/check-password")
    fun check(@AuthenticationPrincipal principal: Principal): ResponseEntity<String> {
        return ResponseEntity.ok("Logged in as ${principal.name}")
    }

}