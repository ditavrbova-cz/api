package cz.ditavrbova.api.shared.service

import cz.ditavrbova.api.configuration.AwsConfiguration
import cz.ditavrbova.api.shared.domain.UploadedFile
import cz.ditavrbova.api.shared.repository.UploadedFileRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.toList
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest
import software.amazon.awssdk.services.s3.model.PutObjectRequest
import java.io.File
import java.time.LocalDateTime
import java.util.*

@Service
class FileService(
    private val s3: S3Client,
    private val configuration: AwsConfiguration,
    private val repository: UploadedFileRepository,
) {

    suspend fun listFiles(): List<UploadedFile> {
        val sort = Sort.by(Sort.Direction.DESC, "uploadedAt")
        val flux = repository.findAll(sort)

        return flux.toList()
    }

    suspend fun uploadFile(file: MultipartFile): UploadedFile {
        val id = UUID.randomUUID()
        val key = "${id}.${File(file.name).extension}"

        val body = RequestBody.fromBytes(file.bytes)
        val request = PutObjectRequest.builder()
            .bucket(configuration.bucket)
            .key(key)
            .acl("public-read")
            .build()

        s3.putObject(request, body)

        val url = "https://${configuration.bucket}.s3.${configuration.region}.amazonaws.com/${key}"
        val document = UploadedFile(id, key, url, LocalDateTime.now())

        return repository.save(document)
    }

    suspend fun deleteFile(id: UUID): Boolean {
        val document = repository.findById(id) ?: return false
        val request = DeleteObjectRequest.builder()
            .bucket(configuration.bucket)
            .key(document.key)
            .build()

        s3.deleteObject(request)
        repository.delete(document)

        return true
    }

    suspend fun deleteFilesByUrls(urls: List<String>) {
        coroutineScope {
            val images = repository.findAllByUrlIn(urls).toList()
            val deferred = images.map {
                async { deleteFile(it.id) }
            }

            deferred.awaitAll()
        }
    }
}