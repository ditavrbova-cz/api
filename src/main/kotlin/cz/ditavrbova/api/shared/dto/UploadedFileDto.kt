package cz.ditavrbova.api.shared.dto

import cz.ditavrbova.api.shared.domain.UploadedFile

data class UploadedFileDto(
    val id: String,
    val url: String
)

fun UploadedFile.toDto() = UploadedFileDto(id.toString(), url)