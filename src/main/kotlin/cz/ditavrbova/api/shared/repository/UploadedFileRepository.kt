package cz.ditavrbova.api.shared.repository

import cz.ditavrbova.api.shared.domain.UploadedFile
import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.data.repository.kotlin.CoroutineSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface UploadedFileRepository : CoroutineCrudRepository<UploadedFile, UUID>, CoroutineSortingRepository<UploadedFile, UUID> {

    fun findAllByUrlIn(urls: List<String>): Flow<UploadedFile>

}