package cz.ditavrbova.api.shared.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime
import java.util.UUID

@Document(collection = "uploadedFiles")
data class UploadedFile(
    @Id
    val id: UUID,
    val key: String,
    val url: String,
    val uploadedAt: LocalDateTime
)