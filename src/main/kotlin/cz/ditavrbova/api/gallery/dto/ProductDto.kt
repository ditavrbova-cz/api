package cz.ditavrbova.api.gallery.dto

import cz.ditavrbova.api.gallery.domain.Product

data class ProductDto(
    val id: String,
    val title: String,
    val description: String,
    val available: Boolean,
    val images: List<String>
)

fun Product.toDto() = ProductDto(
    id.toString(),
    title,
    description,
    available,
    images
)