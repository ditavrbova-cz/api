package cz.ditavrbova.api.gallery.request

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotEmpty

data class CreateProductRequest(
    @NotBlank
    val title: String,
    val description: String,
    val available: Boolean,
    @NotEmpty
    val images: List<String>
)