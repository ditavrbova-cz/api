package cz.ditavrbova.api.gallery.request

data class DeleteProductRequest(val includeImages: Boolean = false)