package cz.ditavrbova.api.gallery.service

import cz.ditavrbova.api.gallery.domain.Product
import cz.ditavrbova.api.gallery.repository.ProductRepository
import cz.ditavrbova.api.shared.service.FileService
import kotlinx.coroutines.flow.toList
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProductService(private val repository: ProductRepository, private val fileService: FileService) {
    suspend fun listProducts(available: Boolean?): List<Product> {
        if (available == null) {
            return repository.findAll().toList()
        }

        return repository.findAllByAvailable(available).toList()
    }

    suspend fun createProduct(title: String, description: String, available: Boolean, images: List<String>): Product {
        val id = UUID.randomUUID()
        val document = Product(id, title, description, available, images)

        return repository.save(document)
    }

    suspend fun updateProduct(id: UUID, title: String, description: String, available: Boolean, images: List<String>): Product? {
        val document = repository.findById(id) ?: return null
        val updated = document.copy(
            title = title,
            description = description,
            available = available,
            images = images
        )

        return repository.save(updated)
    }

    suspend fun deleteProduct(id: UUID, includeImages: Boolean): Boolean {
        val document = repository.findById(id) ?: return false

        repository.delete(document)

        if (includeImages) {
            fileService.deleteFilesByUrls(document.images)
        }

        return true
    }

}