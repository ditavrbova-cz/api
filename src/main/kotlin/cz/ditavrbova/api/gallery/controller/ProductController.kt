package cz.ditavrbova.api.gallery.controller

import cz.ditavrbova.api.gallery.dto.ProductDto
import cz.ditavrbova.api.gallery.dto.toDto
import cz.ditavrbova.api.gallery.request.CreateProductRequest
import cz.ditavrbova.api.gallery.request.DeleteProductRequest
import cz.ditavrbova.api.gallery.response.ProductListingResponse
import cz.ditavrbova.api.gallery.service.ProductService
import jakarta.validation.Valid
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("/api/v1/gallery/products")
class ProductController(private val service: ProductService) {

    @GetMapping
    suspend fun list(@RequestParam("available") available: Boolean?): ResponseEntity<ProductListingResponse> {
        val products = service.listProducts(available)
        val response = ProductListingResponse(
            total = products.size,
            products = products.map { it.toDto() }
        )

        return ResponseEntity.ok(response)
    }

    @PostMapping("/create")
    suspend fun create(@Valid @RequestBody request: CreateProductRequest): ResponseEntity<ProductDto> {
        val product = service.createProduct(request.title, request.description, request.available, request.images)
        val response = product.toDto()

        return ResponseEntity.ok(response)
    }

    @PostMapping("/update/{id}")
    suspend fun update(@PathVariable("id") id: UUID, @Valid @RequestBody request: CreateProductRequest): ResponseEntity<ProductDto> {
        val response = service.updateProduct(id, request.title, request.description, request.available, request.images)
            ?.toDto()
            ?: return ResponseEntity.notFound().build()

        return ResponseEntity.ok(response)
    }

    @PostMapping("/delete/{id}")
    suspend fun delete(@PathVariable("id") id: UUID, @RequestBody request: DeleteProductRequest): ResponseEntity<Unit> {
        return ResponseEntity.status(
            if (service.deleteProduct(id, request.includeImages)) HttpStatus.ACCEPTED
            else HttpStatus.NOT_FOUND
        ).build()
    }

}