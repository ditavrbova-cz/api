package cz.ditavrbova.api.gallery.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.UUID

@Document(collection = "products")
data class Product(
    @Id
    val id: UUID,
    val title: String,
    val description: String,
    val available: Boolean,
    val images: List<String>
)