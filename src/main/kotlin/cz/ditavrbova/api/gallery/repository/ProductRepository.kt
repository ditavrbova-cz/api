package cz.ditavrbova.api.gallery.repository

import cz.ditavrbova.api.gallery.domain.Product
import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface ProductRepository : CoroutineCrudRepository<Product, UUID> {

    fun findAllByAvailable(available: Boolean): Flow<Product>

}