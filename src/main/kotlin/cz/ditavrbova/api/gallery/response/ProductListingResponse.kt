package cz.ditavrbova.api.gallery.response

import cz.ditavrbova.api.gallery.dto.ProductDto

data class ProductListingResponse(
    val total: Int,
    val products: List<ProductDto>
)