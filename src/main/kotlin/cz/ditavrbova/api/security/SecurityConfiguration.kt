package cz.ditavrbova.api.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.config.web.server.invoke
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.util.matcher.PathPatternParserServerWebExchangeMatcher
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.reactive.CorsConfigurationSource
import org.springframework.web.cors.reactive.CorsWebFilter
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource
import org.springframework.web.server.session.WebSessionManager
import reactor.core.publisher.Mono

@Configuration
@EnableWebFluxSecurity
class SecurityConfiguration {

    @Bean
    fun configureSecurityChain(http: ServerHttpSecurity): SecurityWebFilterChain {
        return http {
            cors {}
            csrf { disable() }
            formLogin { disable() }
            httpBasic {}

            authorizeExchange {
                authorize(PathPatternParserServerWebExchangeMatcher("/api/v1/gallery/**", HttpMethod.GET), permitAll)
                authorize(PathPatternParserServerWebExchangeMatcher("/api/v1/blog/post/*", HttpMethod.GET), permitAll)
                authorize(PathPatternParserServerWebExchangeMatcher("/api/v1/**", HttpMethod.GET), hasRole("ADMIN"))
                authorize(PathPatternParserServerWebExchangeMatcher("/api/v1/**", HttpMethod.POST), hasRole("ADMIN"))
                authorize(anyExchange, permitAll)
            }
        }
    }

    @Bean
    fun configureWebSessionManager(): WebSessionManager {
        // Equivalent of Spring MVC SessionCreationPolicy.STATELESS
        return WebSessionManager {
            Mono.empty()
        }
    }
}