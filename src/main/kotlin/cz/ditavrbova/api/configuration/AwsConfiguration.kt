package cz.ditavrbova.api.configuration

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "aws")
data class AwsConfiguration(
    val bucket: String,
    val region: String,
    val accessKeyId: String,
    val accessSecretKey: String
)