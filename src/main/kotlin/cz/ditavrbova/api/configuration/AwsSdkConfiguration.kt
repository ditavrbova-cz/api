package cz.ditavrbova.api.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client

@Configuration
class AwsSdkConfiguration(private val configuration: AwsConfiguration) {
    @Bean
    fun s3(): S3Client {
        val region = Region.of(configuration.region)
        val credentials = AwsBasicCredentials.create(configuration.accessKeyId, configuration.accessSecretKey)
        val credentialsProvider = StaticCredentialsProvider.create(credentials)

        return S3Client.builder()
            .region(region)
            .credentialsProvider(credentialsProvider)
            .build()
    }
}