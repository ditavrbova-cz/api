terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.11.0"
    }
  }

  backend "http" {
    // The terraform state is managed by Gitlab using a CI/CD pipeline
    // See https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html for more info
  }
}

provider "aws" {
  region     = "eu-central-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}


data "aws_iam_policy_document" "image_uploads_access" {
  statement {
    sid     = "ImageUploadsAccess"
    effect  = "Allow"
    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:DeleteObject"
    ]
    resources = [
      aws_s3_bucket.image_uploads.arn
    ]
  }
}

data "aws_iam_policy_document" "image_upload_public_access" {
  statement {
    sid     = "ImageUploadsPublicAccess"
    effect  = "Allow"
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "${aws_s3_bucket.image_uploads.arn}/*"
    ]

    principals {
      identifiers = ["*"]
      type        = "*"
    }
  }
}

resource "aws_iam_policy" "image_uploads_access" {
  name   = "ImageUploadsAccess"
  path   = "/"
  policy = data.aws_iam_policy_document.image_uploads_access.json
}

resource "aws_iam_user" "service_user" {
  name = "DitaVrbovaServiceUser"
}

resource "aws_iam_user_policy_attachment" "service_user_image_uploads_access" {
  policy_arn = aws_iam_policy.image_uploads_access.arn
  user       = aws_iam_user.service_user.name
}

resource "aws_s3_bucket" "image_uploads" {
  bucket = "ditavrbova-cz-image-uploads"
}

// Ensure that the bucket is publicly accessible
resource "aws_s3_bucket_public_access_block" "image_uploads_access" {
  bucket = aws_s3_bucket.image_uploads.id

  block_public_acls   = false
  block_public_policy = false
  ignore_public_acls  = true
}

resource "aws_s3_bucket_policy" "image_uploads_public_access" {
  bucket = aws_s3_bucket.image_uploads.id
  policy = data.aws_iam_policy_document.image_upload_public_access.json
}

variable "aws_access_key" {
  type      = string
  sensitive = true
}

variable "aws_secret_key" {
  type      = string
  sensitive = true
}
